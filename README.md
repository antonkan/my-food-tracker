# Summary

This project was created to

* Have a play ground for technologies
* Create an app that helps me keeping track of what I am eating
* Have some sample / reference usage for different technologies

# Used technologies

| Technologie          | Version  | Link                                                                        |
|----------------------|----------|-----------------------------------------------------------------------------|
| Kotlin               | `1.9.10` | https://kotlinlang.org/docs/releases.html#release-details                   |
| Gradle KTS           | -        | https://docs.gradle.org/current/userguide/kotlin_dsl.html                   |
| Jetpack's Room       | `2.5.2`  | https://developer.android.com/jetpack/androidx/releases/room                |
| Jetpack's Navigation | `2.7.4`  | https://developer.android.com/guide/navigation                              |
| Jetpack's Lifecycle  | `2.5.1`  | https://developer.android.com/jetpack/androidx/releases/lifecycle           |
| Dagger Hilt          | `2.48`   | https://dagger.dev/hilt/                                                    |
| Adapter Delegates    | `4.3.0`  | https://github.com/sockeqwe/AdapterDelegates                                |
| View Binding         |          | https://developer.android.com/topic/libraries/view-binding                  |

# Used patterns

* MVI + MVVM (combined as a state + event + effect approach)

# Screenshots

<img src="readmeres/my-food-overview.png" alt="My Food Ingredients" width="200"/>
<img src="readmeres/my-meals-overview.png" alt="My Meals" width="200"/>
<img src="readmeres/add-ingredient-popup.png" alt="Add Ingredient Popup" width="200"/>

