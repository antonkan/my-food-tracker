// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    id(BuildPlugins.androidApplication).version(BuildPlugins.Versions.androidLibraryVersion)
        .apply(false)
    id(BuildPlugins.androidLibrary).version(BuildPlugins.Versions.androidLibraryVersion)
        .apply(false)
    id(BuildPlugins.kotlinAndroid).version(BuildPlugins.Versions.kotlinVersion).apply(false)
    id(BuildPlugins.ksp).version(BuildPlugins.Versions.kspVersion).apply(false)
    kotlin(BuildPlugins.kapt).version(BuildPlugins.Versions.kaptVersion)
}

buildscript {
    dependencies {
        classpath(BuildPlugins.kotlinGradlePlugin)
        classpath(BuildPlugins.navigationSafeArgsPlugin)
        classpath(BuildPlugins.daggerHiltPlugin)

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

