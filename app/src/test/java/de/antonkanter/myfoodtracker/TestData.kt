package de.antonkanter.myfoodtracker

import de.antonkanter.myfoodtracker.data.DbIngredient
import de.antonkanter.myfoodtracker.data.DbSfiAndIngredient
import de.antonkanter.myfoodtracker.data.DbSingleFoodItem
import de.antonkanter.myfoodtracker.presentation.ingredients.IngredientUiModel

object TestData {
    val testFood1 = DbSingleFoodItem(
        "Something tasty",
        12.7,
        30.9,
        7.6,
    )
    val testFood2 = DbSingleFoodItem(
        "some weird stuff",
        6.6,
        92.8,
        0.6,
    )
    val testFood3 = DbSingleFoodItem(
        "fatty fat fat",
        0.8,
        0.9,
        72.6,
    )
    val testIngredient1 = DbSfiAndIngredient(
        ingredient = DbIngredient(
            98934834783478903,
            30,
            1,
        ),
        sfi = testFood1,
    )
    val testIngredient2 = DbSfiAndIngredient(
        ingredient = DbIngredient(
            847584757454,
            80,
            1
        ),
        sfi = testFood2,
    )
    val testIngredient3 = DbSfiAndIngredient(
        ingredient = DbIngredient(
            837643746348,
            25,
            1
        ),
        sfi = testFood3,
    )
}
