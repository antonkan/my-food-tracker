package de.antonkanter.myfoodtracker.util

import de.antonkanter.myfoodtracker.TestData.testIngredient1
import de.antonkanter.myfoodtracker.TestData.testIngredient2
import de.antonkanter.myfoodtracker.TestData.testIngredient3
import de.antonkanter.myfoodtracker.data.DbSfiAndIngredient
import org.junit.Assert.assertEquals
import org.junit.Test

class DataUtilsTests {
    @Test
    fun `test DataUtils calculateProtein calculates correct protein for 3 items`() {
        val ingredientsList = listOf(
            testIngredient1,
            testIngredient2,
            testIngredient3,
        )

        val protein = ingredientsList.calculateProtein()
        assertEquals(protein, 9.29, 0.0)
    }

    @Test
    fun `test DatUtils calculateProtein calculates correct protein for 0 items`() {
        val ingredientsList = emptyList<DbSfiAndIngredient>()

        val protein = ingredientsList.calculateProtein()
        assertEquals(protein, 0.0, 0.0)
    }

    @Test
    fun `test DataUtils calculateCarbs calculates correct carbs for 3 items`() {
        val ingredientsList = listOf(
            testIngredient1,
            testIngredient2,
            testIngredient3,
        )

        val carbs = ingredientsList.calculateCarbs()
        assertEquals(carbs, 83.735, 0.001)
    }

    @Test
    fun `test DatUtils calculateCarbs calculates correct carbs for 0 items`() {
        val ingredientsList = emptyList<DbSfiAndIngredient>()

        val carbs = ingredientsList.calculateCarbs()
        assertEquals(carbs, 0.0, 0.0)
    }

    @Test
    fun `test DataUtils calculateFat calculates correct fat for 3 items`() {
        val ingredientsList = listOf(
            testIngredient1,
            testIngredient2,
            testIngredient3,
        )

        val fat = ingredientsList.calculateFat()
        assertEquals(fat, 20.91, 0.001)
    }

    @Test
    fun `test DatUtils calculateFat calculates correct fat for 0 items`() {
        val ingredientsList = emptyList<DbSfiAndIngredient>()

        val fat = ingredientsList.calculateFat()
        assertEquals(fat, 0.0, 0.0)
    }
}
