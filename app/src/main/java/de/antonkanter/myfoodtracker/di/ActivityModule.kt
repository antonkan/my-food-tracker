package de.antonkanter.myfoodtracker.di

import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Named

@Module
@InstallIn(ActivityComponent::class)
abstract class ActivityModule {

    companion object {
        @ActivityScoped
        @Provides
        @Named("supportFragmentManager")
        fun provideSupportFragmentManager(activity: FragmentActivity): FragmentManager {
            return activity.supportFragmentManager
        }
    }
}
