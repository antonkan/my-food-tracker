package de.antonkanter.myfoodtracker.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import de.antonkanter.myfoodtracker.data.MftDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(context: Context): MftDatabase {
        return MftDatabase.getDatabase(context)
    }
}
