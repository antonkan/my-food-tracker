package de.antonkanter.myfoodtracker.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import kotlinx.coroutines.flow.Flow

@Dao
interface MealDao {
    @Transaction
    @Query("SELECT * FROM DbMeal")
    fun getAllMeals(): Flow<List<DbMealWithIngredients>>

    @Insert
    suspend fun insertMeal(meal: DbMeal): Long

    @Delete
    suspend fun deleteMeal(meal: DbMeal)
}
