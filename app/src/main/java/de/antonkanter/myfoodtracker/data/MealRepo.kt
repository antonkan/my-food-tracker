package de.antonkanter.myfoodtracker.data

import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class MealRepo @Inject constructor(database: MftDatabase) {
    private val ingredientDao = database.ingredientDao()
    private val mealDao = database.mealDao()

    fun getAllMeals() = mealDao.getAllMeals()

    suspend fun insertMeal(meal: DbMeal, ingredients: List<DbIngredient>) {
        val mealId = mealDao.insertMeal(meal)
        ingredients
            .map { it.copy(mealId = mealId) }
            .forEach { ingredientDao.insertIngredient(it) }
    }

    suspend fun removeMeal(meal: DbMeal) {
        ingredientDao.getAllByMealId(meal.mealId)
            .map { list -> list.forEach { ingredientDao.deleteIngredient(it) } }
            .collect()
        mealDao.deleteMeal(meal)
    }
}
