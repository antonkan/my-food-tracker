package de.antonkanter.myfoodtracker.data

import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class SingleFoodItemRepo @Inject constructor(database: MftDatabase) {

    private val dao: SingleFoodItemDao = database.singleFoodItemDao()

    fun getAllItems() = dao.getAllSingleFoodItems()
    suspend fun addItem(item: DbSingleFoodItem) = dao.insertSingleFoodItem(item)
    suspend fun removeById(id: Long) = dao.getById(id)
        .map {
            it?.let { dao.deleteSingleFoodItem(it) }
        }.collect()
}
