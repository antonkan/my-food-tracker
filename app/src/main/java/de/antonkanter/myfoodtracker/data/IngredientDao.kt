package de.antonkanter.myfoodtracker.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface IngredientDao {

    @Insert
    suspend fun insertIngredient(ingredient: DbIngredient)

    @Query("SELECT * FROM DbIngredient WHERE mealId=:mealId")
    fun getAllByMealId(mealId: Long): Flow<List<DbIngredient>>

    @Delete
    suspend fun deleteIngredient(ingredient: DbIngredient)
}
