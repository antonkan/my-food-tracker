package de.antonkanter.myfoodtracker.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface SingleFoodItemDao {
    @Insert
    suspend fun insertSingleFoodItem(singleFoodItem: DbSingleFoodItem)

    @Delete
    suspend fun deleteSingleFoodItem(singleFoodItem: DbSingleFoodItem)

    @Query("SELECT * FROM DbSingleFoodItem")
    fun getAllSingleFoodItems(): Flow<List<DbSingleFoodItem>>

    @Query("SELECT * FROM DbSingleFoodItem WHERE sfiId=:id")
    fun getById(id: Long): Flow<DbSingleFoodItem?>
}
