package de.antonkanter.myfoodtracker.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [DbSingleFoodItem::class, DbIngredient::class, DbMeal::class],
    version = 1,
    exportSchema = false
)
abstract class MftDatabase : RoomDatabase() {

    abstract fun singleFoodItemDao(): SingleFoodItemDao
    abstract fun ingredientDao(): IngredientDao
    abstract fun mealDao(): MealDao

    companion object {
        @Volatile
        private var INSTANCE: MftDatabase? = null

        fun getDatabase(context: Context): MftDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MftDatabase::class.java,
                    "mft_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
