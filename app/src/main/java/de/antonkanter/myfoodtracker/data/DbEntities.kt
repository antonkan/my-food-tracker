package de.antonkanter.myfoodtracker.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity
data class DbSingleFoodItem(
    val name: String,
    val proteinPerHundred: Double,
    val carbsPerHundred: Double,
    val fatPerHundred: Double,
    @PrimaryKey(autoGenerate = true) var sfiId: Long = 0,
)

@Entity
data class DbIngredient(
    val sfiId: Long,
    val amountInG: Int,
    val mealId: Long,
    @PrimaryKey(autoGenerate = true) var ingredientId: Long = 0,
)

data class DbSfiAndIngredient(
    @Embedded val ingredient: DbIngredient,
    @Relation(
        parentColumn = "sfiId",
        entityColumn = "sfiId"
    )
    val sfi: DbSingleFoodItem,
)

@Entity
data class DbMeal(
    val name: String,
    @PrimaryKey(autoGenerate = true) var mealId: Long = 0,
)

data class DbMealWithIngredients(
    @Embedded val meal: DbMeal,
    @Relation(
        parentColumn = "mealId",
        entityColumn = "mealId",
        entity = DbIngredient::class,
    )
    val ingredients: List<DbSfiAndIngredient>,
)
