package de.antonkanter.myfoodtracker.util

import android.view.View

fun View.onClick(listener: ((View) -> Unit)?) = setOnClickListener(listener)
