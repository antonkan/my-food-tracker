package de.antonkanter.myfoodtracker.util

import de.antonkanter.myfoodtracker.data.DbSfiAndIngredient

fun List<DbSfiAndIngredient>.calculateProtein() =
    sumOf { it.sfi.proteinPerHundred * it.ingredient.amountInG / 100.toDouble() }

fun List<DbSfiAndIngredient>.calculateCarbs() =
    sumOf { it.sfi.carbsPerHundred * it.ingredient.amountInG / 100.toDouble() }

fun List<DbSfiAndIngredient>.calculateFat() =
    sumOf { it.sfi.fatPerHundred * it.ingredient.amountInG / 100.toDouble() }

fun List<DbSfiAndIngredient>.calculateWeight() =
    sumOf { it.ingredient.amountInG }
