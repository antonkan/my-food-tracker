package de.antonkanter.myfoodtracker

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MFTApplication : Application() {
}
