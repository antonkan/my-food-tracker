package de.antonkanter.myfoodtracker.presentation.mymeals

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.antonkanter.myfoodtracker.data.MealRepo
import de.antonkanter.myfoodtracker.presentation.base.MviViewModel
import de.antonkanter.myfoodtracker.presentation.mymeals.model.MealItem
import de.antonkanter.myfoodtracker.presentation.mymeals.model.MyMealsEffect
import de.antonkanter.myfoodtracker.presentation.mymeals.model.MyMealsEvent
import de.antonkanter.myfoodtracker.presentation.mymeals.model.MyMealsState
import de.antonkanter.myfoodtracker.presentation.mymeals.model.toDb
import de.antonkanter.myfoodtracker.presentation.mymeals.model.toMealItem
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyMealsViewModel @Inject constructor(
    val savedStateHandle: SavedStateHandle,
    private val mealRepo: MealRepo,
) : MviViewModel<MyMealsState, MyMealsEvent, MyMealsEffect>(MyMealsState.Loading) {

    override fun processEvent(viewEvent: MyMealsEvent) {
        when (viewEvent) {
            MyMealsEvent.OnInit -> onInit()
            MyMealsEvent.OnFabClicked -> {
                viewEffect = MyMealsEffect.NavigateToCreateMeal
            }
            is MyMealsEvent.OnMealItemDeleteClick -> deleteMealItem(viewEvent.mealItem)
        }
    }

    private fun fetchMeals() {
        viewModelScope.launch {
            suspendFetchMeals()
        }
    }

    private fun setLoading() {
        viewState = MyMealsState.Loading
    }

    private fun setSuccess(items: List<MealItem>) {
        viewState = MyMealsState.Loaded(items)
    }

    private fun onInit() {
        setLoading()
        fetchMeals()
    }

    private fun deleteMealItem(mealItem: MealItem) {
        viewModelScope.launch {
            mealRepo.removeMeal(mealItem.toDb())
            suspendFetchMeals()
            viewEffect = MyMealsEffect.MealRemoved
        }
    }

    private suspend fun suspendFetchMeals() {
        mealRepo.getAllMeals()
            .map { list -> list.map { it.toMealItem() } }
            .collectLatest { setSuccess(it) }
    }
}
