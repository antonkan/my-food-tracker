package de.antonkanter.myfoodtracker.presentation.myfood

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import dagger.hilt.android.scopes.FragmentScoped
import de.antonkanter.myfoodtracker.presentation.myfood.delegates.SingleFoodItemDelegate
import de.antonkanter.myfoodtracker.presentation.myfood.model.MyFoodItem
import de.antonkanter.myfoodtracker.presentation.myfood.model.SingleFoodItem
import javax.inject.Inject

@FragmentScoped
class MyFoodAdapter @Inject constructor(
    private val singleFoodItemDelegate: SingleFoodItemDelegate
) : AsyncListDifferDelegationAdapter<MyFoodItem>(DiffUtilCallback) {

    init {
        delegatesManager
            .addDelegate(singleFoodItemDelegate)
    }

    fun onDeleteItemClick(onDeleteClick: (SingleFoodItem) -> Unit) {
        singleFoodItemDelegate.onDeleteClick = onDeleteClick
    }

    companion object DiffUtilCallback : DiffUtil.ItemCallback<MyFoodItem>() {
        override fun areItemsTheSame(oldItem: MyFoodItem, newItem: MyFoodItem) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: MyFoodItem, newItem: MyFoodItem) =
            oldItem == newItem
    }
}
