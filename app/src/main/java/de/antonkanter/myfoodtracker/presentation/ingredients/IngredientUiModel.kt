package de.antonkanter.myfoodtracker.presentation.ingredients

import de.antonkanter.myfoodtracker.data.DbIngredient
import de.antonkanter.myfoodtracker.data.DbSfiAndIngredient

data class IngredientUiModel(
    val sfiId: Long,
    val name: String,
    val amountInG: Int,
    val ingredientId: Long
)

fun DbSfiAndIngredient.toIngredientItem() =
    IngredientUiModel(
        sfiId = sfi.sfiId,
        name = sfi.name,
        amountInG = ingredient.amountInG,
        ingredientId = ingredient.ingredientId
    )

fun IngredientUiModel.toDb() =
    DbIngredient(
        sfiId = sfiId,
        amountInG = amountInG,
        mealId = -1 // will be applied on insert
    )
