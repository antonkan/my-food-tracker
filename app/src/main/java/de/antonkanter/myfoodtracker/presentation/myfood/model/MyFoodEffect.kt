package de.antonkanter.myfoodtracker.presentation.myfood.model

sealed class MyFoodEffect {
    object ItemAdded : MyFoodEffect()
    object OpenAddItemDialog : MyFoodEffect()
    object ItemRemoved : MyFoodEffect()
}
