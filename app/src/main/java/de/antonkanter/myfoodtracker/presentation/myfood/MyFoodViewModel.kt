package de.antonkanter.myfoodtracker.presentation.myfood

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.antonkanter.myfoodtracker.data.SingleFoodItemRepo
import de.antonkanter.myfoodtracker.presentation.base.MviViewModel
import de.antonkanter.myfoodtracker.presentation.myfood.model.MyFoodEffect
import de.antonkanter.myfoodtracker.presentation.myfood.model.MyFoodEvent
import de.antonkanter.myfoodtracker.presentation.myfood.model.MyFoodItem
import de.antonkanter.myfoodtracker.presentation.myfood.model.MyFoodState
import de.antonkanter.myfoodtracker.presentation.myfood.model.SingleFoodItem
import de.antonkanter.myfoodtracker.presentation.myfood.model.toDb
import de.antonkanter.myfoodtracker.presentation.myfood.model.toFoodItem
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyFoodViewModel @Inject constructor(
    val savedStateHandle: SavedStateHandle,
    private val sfiRepo: SingleFoodItemRepo,
) : MviViewModel<MyFoodState, MyFoodEvent, MyFoodEffect>(MyFoodState.Loading) {

    init {
        setLoading()
        fetchFoodItems()
    }

    override fun processEvent(viewEvent: MyFoodEvent) {
        when (viewEvent) {
            MyFoodEvent.OnAddItemClicked -> openAddItemDialog()
            is MyFoodEvent.OnAddItemDialogClicked -> addFoodItem(
                viewEvent.titleInput,
                viewEvent.proteinValueInput.toDouble(),
                viewEvent.carbsValueInput.toDouble(),
                viewEvent.fatValueInput.toDouble(),
            )
            is MyFoodEvent.OnRemoveItemClicked -> removeItem(viewEvent.item)
        }
    }

    private fun fetchFoodItems() {
        viewModelScope.launch {
            suspendFetchFoodItems()
        }
    }

    private suspend fun suspendFetchFoodItems() {
        sfiRepo.getAllItems()
            .map { list -> list.map { it.toFoodItem() } }
            .collectLatest { setSuccess(it) }
    }

    private fun openAddItemDialog() {
        viewEffect = MyFoodEffect.OpenAddItemDialog
    }

    private fun addFoodItem(
        title: String,
        proteinValue: Double,
        carbsValue: Double,
        fatValue: Double
    ) {
        val newItem = SingleFoodItem(title, proteinValue, carbsValue, fatValue)
        viewModelScope.launch {
            sfiRepo.addItem(newItem.toDb())
            suspendFetchFoodItems()
            viewEffect = MyFoodEffect.ItemAdded
        }
    }

    private fun removeItem(item: SingleFoodItem) {
        viewModelScope.launch {
            sfiRepo.removeById(item.id)
            suspendFetchFoodItems()
            viewEffect = MyFoodEffect.ItemRemoved
        }
    }

    private fun setLoading() {
        viewState = MyFoodState.Loading
    }

    private fun setSuccess(items: List<MyFoodItem>) {
        viewState = MyFoodState.Loaded(items)
    }
}
