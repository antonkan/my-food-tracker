package de.antonkanter.myfoodtracker.presentation.myfood

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import de.antonkanter.myfoodtracker.R
import de.antonkanter.myfoodtracker.databinding.MyFoodFragmentBinding
import de.antonkanter.myfoodtracker.presentation.base.MviFragment
import de.antonkanter.myfoodtracker.presentation.myfood.additem.AddFoodItemDialogFragment
import de.antonkanter.myfoodtracker.presentation.myfood.model.MyFoodEffect
import de.antonkanter.myfoodtracker.presentation.myfood.model.MyFoodEvent
import de.antonkanter.myfoodtracker.presentation.myfood.model.MyFoodState
import de.antonkanter.myfoodtracker.presentation.util.SimpleDividerItemDecoration
import de.antonkanter.myfoodtracker.util.onClick
import javax.inject.Inject
import javax.inject.Named

private typealias ViewBinding = MyFoodFragmentBinding
private typealias State = MyFoodState
private typealias Effect = MyFoodEffect
private typealias Event = MyFoodEvent
private typealias ViewModel = MyFoodViewModel

@AndroidEntryPoint
class MyFoodFragment : MviFragment<ViewBinding, State, Effect, Event, ViewModel>() {

    @Inject
    internal lateinit var adapter: MyFoodAdapter

    @Inject
    @Named("supportFragmentManager")
    internal lateinit var supportFragmentManager: FragmentManager

    companion object {
        fun newInstance() = MyFoodFragment()
    }

    override val viewModel: MyFoodViewModel by activityViewModels()

    override fun inflateViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        ViewBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecView()
        setupClickListener()
    }

    override fun renderViewState(viewState: State) {
        when (viewState) {
            is MyFoodState.Loading -> {
                // TODO
            }
            is MyFoodState.Loaded -> adapter.items = viewState.items
            is MyFoodState.Error -> {
                // TODO
            }
        }
    }

    override fun renderViewEffect(viewEffect: Effect) {
        when (viewEffect) {
            MyFoodEffect.OpenAddItemDialog -> navigateToAddItemDialog()
            MyFoodEffect.ItemAdded ->
                Snackbar.make(binding.myFoodRoot, "Item added", Snackbar.LENGTH_SHORT).show()
            MyFoodEffect.ItemRemoved ->
                Snackbar.make(binding.myFoodRoot, "Item removed", Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun setupRecView() {
        binding.myFoodRecView.apply {
            layoutManager = LinearLayoutManager(context, VERTICAL, false)
            adapter = this@MyFoodFragment.adapter.apply {
                onDeleteItemClick {
                    processViewEvent(MyFoodEvent.OnRemoveItemClicked(it))
                }
            }
            addItemDecoration(
                SimpleDividerItemDecoration(R.dimen.food_rec_items_vertical_space, context)
            )
        }
    }

    private fun setupClickListener() {
        binding.myFoodAddFab.onClick {
            processViewEvent(MyFoodEvent.OnAddItemClicked)
        }
    }

    private fun navigateToAddItemDialog() {
        AddFoodItemDialogFragment
            .newInstance()
            .show(supportFragmentManager, AddFoodItemDialogFragment.TAG)
    }
}
