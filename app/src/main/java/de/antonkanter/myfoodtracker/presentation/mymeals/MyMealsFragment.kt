package de.antonkanter.myfoodtracker.presentation.mymeals

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import de.antonkanter.myfoodtracker.R
import de.antonkanter.myfoodtracker.databinding.MyMealsFragmentBinding
import de.antonkanter.myfoodtracker.presentation.base.MviFragment
import de.antonkanter.myfoodtracker.presentation.mymeals.model.MyMealsEffect
import de.antonkanter.myfoodtracker.presentation.mymeals.model.MyMealsEvent
import de.antonkanter.myfoodtracker.presentation.mymeals.model.MyMealsState
import de.antonkanter.myfoodtracker.presentation.util.SimpleDividerItemDecoration
import de.antonkanter.myfoodtracker.util.onClick
import javax.inject.Inject

private typealias ViewBinding = MyMealsFragmentBinding
private typealias State = MyMealsState
private typealias Effect = MyMealsEffect
private typealias Event = MyMealsEvent
private typealias ViewModel = MyMealsViewModel

@AndroidEntryPoint
class MyMealsFragment : MviFragment<ViewBinding, State, Effect, Event, ViewModel>() {
    @Inject
    internal lateinit var adapter: MyMealsAdapter

    override val viewModel: MyMealsViewModel by activityViewModels()

    override fun inflateViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        ViewBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecView()
        binding.myMealsAddFab.onClick {
            processViewEvent(MyMealsEvent.OnFabClicked)
        }
        processViewEvent(MyMealsEvent.OnInit)
    }

    override fun renderViewState(viewState: State) {
        when (viewState) {
            is MyMealsState.Loading -> {
                // TODO
            }
            is MyMealsState.Loaded -> {
                adapter.items = viewState.items
            }
        }
    }

    override fun renderViewEffect(viewEffect: Effect) {
        when (viewEffect) {
            MyMealsEffect.NavigateToCreateMeal -> findNavController().navigate(
                MyMealsFragmentDirections.actionMyMealsFragmentToCreateMealFragment()
            )
            MyMealsEffect.MealRemoved ->
                Snackbar.make(binding.myMealsRoot, "Meal Removed", Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun setupRecView() {
        binding.myMealsRecView.apply {
            layoutManager = LinearLayoutManager(context, VERTICAL, false)
            adapter = this@MyMealsFragment.adapter.apply {
                onDeleteItemClick {
                    processViewEvent(
                        MyMealsEvent.OnMealItemDeleteClick(it)
                    )
                }
            }
            addItemDecoration(
                SimpleDividerItemDecoration(R.dimen.food_rec_items_vertical_space, context)
            )
        }
    }
}
