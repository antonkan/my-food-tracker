package de.antonkanter.myfoodtracker.presentation.createmeal.model

import de.antonkanter.myfoodtracker.data.DbSingleFoodItem

data class FoodItemChoice(
    val sfiId: Long,
    val name: String
)

fun DbSingleFoodItem.toFoodItemChoice() =
    FoodItemChoice(
        sfiId = sfiId,
        name = name
    )
