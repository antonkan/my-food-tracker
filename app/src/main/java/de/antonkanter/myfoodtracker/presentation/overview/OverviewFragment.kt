package de.antonkanter.myfoodtracker.presentation.overview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import de.antonkanter.myfoodtracker.databinding.OverviewFragmentBinding
import de.antonkanter.myfoodtracker.util.onClick

class OverviewFragment : Fragment() {

    companion object {
        fun newInstance() =
            OverviewFragment()
    }

    private var _binding: OverviewFragmentBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = OverviewFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.overviewFragmentButtonFood.onClick {
            findNavController().navigate(
                OverviewFragmentDirections.actionOverviewFragmentToMyFoodFragment()
            )
        }
        binding.overviewFragmentButtonMeals.onClick {
            findNavController().navigate(
                OverviewFragmentDirections.actionOverviewFragmentToMyMealsFragment()
            )
        }
    }
}
