package de.antonkanter.myfoodtracker.presentation.myfood.additem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import dagger.hilt.android.AndroidEntryPoint
import de.antonkanter.myfoodtracker.databinding.AddFoodItemFragmentBinding
import de.antonkanter.myfoodtracker.presentation.base.CustomBottomSheetDialogFragment
import de.antonkanter.myfoodtracker.presentation.myfood.MyFoodViewModel
import de.antonkanter.myfoodtracker.presentation.myfood.model.MyFoodEvent
import de.antonkanter.myfoodtracker.util.onClick

@AndroidEntryPoint
class AddFoodItemDialogFragment : CustomBottomSheetDialogFragment<AddFoodItemFragmentBinding>() {

    private val viewModel: MyFoodViewModel by activityViewModels()

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = AddFoodItemFragmentBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.addFoodItemCta.onClick {
            viewModel.processEvent(
                MyFoodEvent.OnAddItemDialogClicked(
                    titleInput = binding.addFoodItemTitleTextInput.text.toString(),
                    proteinValueInput = binding.addFoodItemProteinValue.text.toString(),
                    carbsValueInput = binding.addFoodItemCarbsValue.text.toString(),
                    fatValueInput = binding.addFoodItemFatValue.text.toString(),
                )
            )
            dismiss()
        }
    }

    companion object {
        const val TAG = "AddItemDialogFragment"
        fun newInstance() = AddFoodItemDialogFragment()
    }
}
