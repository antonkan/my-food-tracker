package de.antonkanter.myfoodtracker.presentation.myfood.delegates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import dagger.hilt.android.scopes.FragmentScoped
import de.antonkanter.myfoodtracker.R
import de.antonkanter.myfoodtracker.databinding.MyFoodSingleFoodItemViewBinding
import de.antonkanter.myfoodtracker.presentation.myfood.model.MyFoodItem
import de.antonkanter.myfoodtracker.presentation.myfood.model.SingleFoodItem
import de.antonkanter.myfoodtracker.util.onClick
import javax.inject.Inject

@FragmentScoped
class SingleFoodItemDelegate @Inject constructor() : AbsListItemAdapterDelegate<
        SingleFoodItem, MyFoodItem, SingleFoodItemDelegate.SingleFoodItemViewHolder>() {

    lateinit var onDeleteClick: (SingleFoodItem) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup): SingleFoodItemViewHolder =
        LayoutInflater.from(parent.context)
            .inflate(R.layout.my_food_single_food_item_view, parent, false)
            .let { SingleFoodItemViewHolder(it) }

    override fun isForViewType(
        item: MyFoodItem,
        items: MutableList<MyFoodItem>,
        position: Int
    ): Boolean = item is SingleFoodItem

    override fun onBindViewHolder(
        item: SingleFoodItem,
        holder: SingleFoodItemViewHolder,
        payloads: MutableList<Any>
    ) = holder.bind(item, onDeleteClick)

    class SingleFoodItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val binding = MyFoodSingleFoodItemViewBinding.bind(itemView)

        fun bind(item: SingleFoodItem, onDeleteClick: (SingleFoodItem) -> Unit) = with(binding) {
            sfiName.text = item.name
            sfiProteinValue.text = itemView.context.getString(
                R.string.my_food_single_food_item_protein_value,
                item.proteinPerHundred
            )
            sfiCarbsValue.text = itemView.context.getString(
                R.string.my_food_single_food_item_carbs_value,
                item.carbsPerHundred
            )
            sfiFatValue.text = itemView.context.getString(
                R.string.my_food_single_food_item_fat_value,
                item.fatPerHundred
            )
            sfiDeleteIcon.onClick { onDeleteClick(item) }
        }
    }
}
