package de.antonkanter.myfoodtracker.presentation.ingredients

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.setPadding
import de.antonkanter.myfoodtracker.R

class IngredientsContainerView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null
) : LinearLayout(context, attributeSet) {

    init {
        orientation = VERTICAL
        setPadding(context.resources.getDimension(R.dimen.size_m).toInt())
        bind(emptyList())
    }

    fun bind(items: List<IngredientUiModel>) {
        removeAllViews()
        if (items.isEmpty()) {
            addView(TextView(context).apply { text = "Empty" })
        } else {
            items.forEach {
                addView(
                    IngredientItemView(context).apply { bind(it) }
                )
            }
        }
    }
}
