package de.antonkanter.myfoodtracker.presentation.createmeal.addingredient

import android.app.AlertDialog
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import de.antonkanter.myfoodtracker.databinding.AddIngredientToMealFragmentBinding
import de.antonkanter.myfoodtracker.presentation.base.CustomBottomSheetDialogFragment
import de.antonkanter.myfoodtracker.presentation.createmeal.CreateMealViewModel
import de.antonkanter.myfoodtracker.presentation.createmeal.model.CreateMealEvent
import de.antonkanter.myfoodtracker.presentation.createmeal.model.FoodItemChoice
import de.antonkanter.myfoodtracker.util.onClick
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import kotlin.properties.Delegates

@AndroidEntryPoint
class AddIngredientDialogFragment :
    CustomBottomSheetDialogFragment<AddIngredientToMealFragmentBinding>() {

    private val viewModel: CreateMealViewModel by activityViewModels()

    private var newIngredientFood by Delegates.observable<FoodItemChoice?>(null) { _, _, newValue ->
        binding.addIngredientTypeSelector.text = SpannableStringBuilder(newValue?.name)
    }

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = AddIngredientToMealFragmentBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupOnNameClickListener()
        setupOnAddClickListener()
    }

    private fun setupOnNameClickListener() =
        binding.addIngredientTypeSelector.onClick {
            lifecycleScope.launch {
                // TODO show loading spinner first until items are loaded and shown
                viewModel.allFoodItems.filterNotNull().collectLatest { foodItems ->
                    AlertDialog.Builder(requireActivity())
                        .setTitle("Pick the ingredient")
                        .setItems(foodItems.map { it.name }.toTypedArray()) { dialog, which ->
                            newIngredientFood = foodItems.getOrNull(which)
                            dialog.dismiss()
                        }
                        .create()
                        .show()
                }
            }
        }

    private fun setupOnAddClickListener() =
        binding.addIngredientCta.onClick {
            newIngredientFood?.let {
                viewModel.processEvent(
                    CreateMealEvent.OnAddIngredientFinished(
                        sfiId = it.sfiId,
                        name = it.name,
                        amount = binding.addIngredientAmountTextInput.text.toString().toInt(),
                        dbId = it.sfiId
                    )
                )
                dismiss()
            } ?: run {
                // TODO process error "no item selected"
            }
        }

    companion object {
        const val TAG = "AddIngredientDialogFragment"
        fun newInstance() = AddIngredientDialogFragment()
    }
}
