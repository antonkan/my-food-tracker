package de.antonkanter.myfoodtracker.presentation.createmeal.model

import de.antonkanter.myfoodtracker.presentation.ingredients.IngredientUiModel

sealed class CreateMealState {
    abstract val ingredients: List<IngredientUiModel>

    object Loading : CreateMealState() {
        override val ingredients = emptyList<IngredientUiModel>()
    }

    data class Loaded(
        override val ingredients: List<IngredientUiModel>
    ) : CreateMealState()
}
