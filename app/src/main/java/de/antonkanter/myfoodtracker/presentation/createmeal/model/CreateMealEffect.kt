package de.antonkanter.myfoodtracker.presentation.createmeal.model

sealed class CreateMealEffect {
    object NavigateToAddIngredient : CreateMealEffect()
    object ShowMissingTitleError : CreateMealEffect()
    object ShowEmptyIngredientsError : CreateMealEffect()
    object MealAdded : CreateMealEffect()
}
