package de.antonkanter.myfoodtracker.presentation.mymeals.model

import de.antonkanter.myfoodtracker.data.DbMeal
import de.antonkanter.myfoodtracker.data.DbMealWithIngredients
import de.antonkanter.myfoodtracker.presentation.ingredients.IngredientUiModel
import de.antonkanter.myfoodtracker.presentation.ingredients.toIngredientItem
import de.antonkanter.myfoodtracker.util.calculateCarbs
import de.antonkanter.myfoodtracker.util.calculateFat
import de.antonkanter.myfoodtracker.util.calculateProtein
import de.antonkanter.myfoodtracker.util.calculateWeight

sealed class MyMealsItem {
    abstract val id: Long
}

data class MealItem(
    val name: String,
    val weightInG: Int,
    val proteinInG: Double,
    val carbsInG: Double,
    val fatInG: Double,
    val ingredients: List<IngredientUiModel>,
    override val id: Long = -1 // will be set from db
) : MyMealsItem()

fun DbMealWithIngredients.toMealItem() =
    MealItem(
        id = meal.mealId,
        name = meal.name,
        weightInG = ingredients.calculateWeight(),
        proteinInG = ingredients.calculateProtein(),
        carbsInG = ingredients.calculateCarbs(),
        fatInG = ingredients.calculateFat(),
        ingredients = ingredients.map {
            it.toIngredientItem()
        }
    )

fun MealItem.toDb() = DbMeal(name, id)
