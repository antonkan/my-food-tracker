package de.antonkanter.myfoodtracker.presentation.myfood.model

sealed class MyFoodState {

    object Loading: MyFoodState()

    data class Loaded(
        val items: List<MyFoodItem>
    ): MyFoodState()

    object Error : MyFoodState()
}
