package de.antonkanter.myfoodtracker.presentation.mymeals

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import dagger.hilt.android.scopes.FragmentScoped
import de.antonkanter.myfoodtracker.presentation.mymeals.delegates.MealItemDelegate
import de.antonkanter.myfoodtracker.presentation.mymeals.model.MealItem
import de.antonkanter.myfoodtracker.presentation.mymeals.model.MyMealsItem
import javax.inject.Inject

@FragmentScoped
class MyMealsAdapter @Inject constructor(
    private val mealItemDelegate: MealItemDelegate
) : AsyncListDifferDelegationAdapter<MyMealsItem>(DiffUtilCallback) {

    init {
        delegatesManager.addDelegate(mealItemDelegate)
    }

    fun onDeleteItemClick(onDeleteClick: (MealItem) -> Unit) {
        mealItemDelegate.onDeleteClick = onDeleteClick
    }

    companion object DiffUtilCallback : DiffUtil.ItemCallback<MyMealsItem>() {
        override fun areItemsTheSame(oldItem: MyMealsItem, newItem: MyMealsItem) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: MyMealsItem, newItem: MyMealsItem) =
            oldItem == newItem
    }
}
