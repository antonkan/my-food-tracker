package de.antonkanter.myfoodtracker.presentation.mymeals.model

sealed class MyMealsEffect {
    object NavigateToCreateMeal : MyMealsEffect()
    object MealRemoved : MyMealsEffect()
}
