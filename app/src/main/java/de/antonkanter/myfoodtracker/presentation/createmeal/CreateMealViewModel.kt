package de.antonkanter.myfoodtracker.presentation.createmeal

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.antonkanter.myfoodtracker.data.DbMeal
import de.antonkanter.myfoodtracker.data.MealRepo
import de.antonkanter.myfoodtracker.data.SingleFoodItemRepo
import de.antonkanter.myfoodtracker.presentation.base.MviViewModel
import de.antonkanter.myfoodtracker.presentation.createmeal.model.CreateMealEffect
import de.antonkanter.myfoodtracker.presentation.createmeal.model.CreateMealEvent
import de.antonkanter.myfoodtracker.presentation.createmeal.model.CreateMealState
import de.antonkanter.myfoodtracker.presentation.createmeal.model.FoodItemChoice
import de.antonkanter.myfoodtracker.presentation.createmeal.model.toFoodItemChoice
import de.antonkanter.myfoodtracker.presentation.ingredients.IngredientUiModel
import de.antonkanter.myfoodtracker.presentation.ingredients.toDb
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CreateMealViewModel @Inject constructor(
    val savedStateHandle: SavedStateHandle,
    singleFoodItemRepo: SingleFoodItemRepo,
    private val mealRepo: MealRepo
) : MviViewModel<CreateMealState, CreateMealEvent, CreateMealEffect>(CreateMealState.Loading) {

    val allFoodItems: Flow<List<FoodItemChoice>?> = singleFoodItemRepo.getAllItems()
        .map { list -> list.map { it.toFoodItemChoice() } }
        .stateIn(viewModelScope, SharingStarted.Eagerly, null)
    // TODO (#9) enable interactions when food is successfully loaded

    override fun processEvent(viewEvent: CreateMealEvent) {
        when (viewEvent) {
            CreateMealEvent.OnInit -> Unit
            CreateMealEvent.OnAddIngredientClicked -> onAddIngredientClicked()
            is CreateMealEvent.OnAddIngredientFinished ->
                onAddIngredientFinished(
                    viewEvent.sfiId,
                    viewEvent.name,
                    viewEvent.amount,
                    viewEvent.dbId
                )
            is CreateMealEvent.OnFinishAddMeal -> onFinishAddMeal(viewEvent.titleInput)
        }
    }

    private fun onAddIngredientClicked() {
        viewEffect = CreateMealEffect.NavigateToAddIngredient
    }

    private fun onAddIngredientFinished(sfiId: Long, name: String, amount: Int, foodDbId: Long) {
        val newItem = IngredientUiModel(
            sfiId = sfiId,
            name = name,
            amountInG = amount,
            ingredientId = foodDbId
        )
        viewState = CreateMealState.Loaded(
            ingredients = viewState.ingredients + newItem
        )
    }

    private fun onFinishAddMeal(titleInput: String?) {
        titleInput ?: run {
            viewEffect = CreateMealEffect.ShowMissingTitleError
            return
        }

        if (viewState.ingredients.isEmpty()) {
            viewEffect = CreateMealEffect.ShowEmptyIngredientsError
        }

        val dbIngredients = viewState.ingredients
            .map { it.toDb() }
        val dbMeal = DbMeal(titleInput)

        viewModelScope.launch {
            mealRepo.insertMeal(dbMeal, dbIngredients)
            viewEffect = CreateMealEffect.MealAdded
        }
    }
}
