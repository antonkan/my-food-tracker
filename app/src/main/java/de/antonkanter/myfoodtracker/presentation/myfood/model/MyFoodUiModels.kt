package de.antonkanter.myfoodtracker.presentation.myfood.model

import de.antonkanter.myfoodtracker.data.DbSingleFoodItem

sealed class MyFoodItem {
    abstract val id: Long
}

data class SingleFoodItem(
    val name: String,
    val proteinPerHundred: Double,
    val carbsPerHundred: Double,
    val fatPerHundred: Double,
    override val id: Long = -1, //will be initialized by db
) : MyFoodItem()

fun DbSingleFoodItem.toFoodItem() =
    SingleFoodItem(
        id = sfiId,
        name = name,
        proteinPerHundred = proteinPerHundred,
        carbsPerHundred = carbsPerHundred,
        fatPerHundred = fatPerHundred,
    )

fun SingleFoodItem.toDb() = DbSingleFoodItem(
    name = name,
    proteinPerHundred = proteinPerHundred,
    carbsPerHundred = carbsPerHundred,
    fatPerHundred = fatPerHundred,
)
