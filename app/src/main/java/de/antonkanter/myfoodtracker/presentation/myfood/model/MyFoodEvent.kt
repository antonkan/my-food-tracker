package de.antonkanter.myfoodtracker.presentation.myfood.model

sealed class MyFoodEvent {
    object OnAddItemClicked : MyFoodEvent()
    data class OnAddItemDialogClicked(
        val titleInput: String,
        val proteinValueInput: String,
        val carbsValueInput: String,
        val fatValueInput: String,
    ) : MyFoodEvent()

    data class OnRemoveItemClicked(val item: SingleFoodItem) : MyFoodEvent()
}
