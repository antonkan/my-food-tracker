package de.antonkanter.myfoodtracker.presentation.createmeal.model

sealed class CreateMealEvent {
    object OnInit : CreateMealEvent()
    object OnAddIngredientClicked : CreateMealEvent()
    data class OnAddIngredientFinished(
        val sfiId: Long,
        val name: String,
        val amount: Int,
        val dbId: Long
    ) : CreateMealEvent()

    data class OnFinishAddMeal(val titleInput: String?) : CreateMealEvent()
}
