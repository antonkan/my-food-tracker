package de.antonkanter.myfoodtracker.presentation.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

abstract class MviViewModel<STATE, EVENT, EFFECT>(initialState: STATE) : ViewModel() {

    private val viewStates = MutableStateFlow(initialState)

    protected var viewState: STATE
        get() = viewStates.value
        set(value) { viewStates.value = value }

    private val viewEffects = MutableSharedFlow<EFFECT>()

    private var _viewEffect: EFFECT? = null
    protected var viewEffect: EFFECT
        get() = _viewEffect
            ?: throw UninitializedPropertyAccessException("\"viewEffect\" was accessed before being initialized")
        set(value) {
            _viewEffect = value
            viewModelScope.launch { viewEffects.emit(value) }
        }

    abstract fun processEvent(viewEvent: EVENT)

    fun onState(listener: (state: STATE) -> Unit) {
        viewModelScope.launch {
            viewStates.collect { listener(it) }
        }
    }

    fun onEffect(listener: (effect: EFFECT) -> Unit) {
        viewModelScope.launch {
            viewEffects.collect { listener(it) }
        }
    }
}
