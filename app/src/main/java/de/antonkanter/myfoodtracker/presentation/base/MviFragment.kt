package de.antonkanter.myfoodtracker.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class MviFragment<
        BINDING : ViewBinding, STATE, EFFECT, EVENT, VM : MviViewModel<STATE, EVENT, EFFECT>>:
    Fragment() {

    abstract val viewModel: VM

    private var _binding: BINDING? = null
    // This property is only valid between onCreateView and onDestroyView.
    protected val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = inflateViewBinding(inflater, container)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onState { renderViewState(it) }
        viewModel.onEffect { renderViewEffect(it) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun processViewEvent(event: EVENT) {
        viewModel.processEvent(event)
    }

    abstract fun inflateViewBinding(inflater: LayoutInflater, container: ViewGroup?) : BINDING
    abstract fun renderViewState(viewState: STATE)
    abstract fun renderViewEffect(viewEffect: EFFECT)
}
