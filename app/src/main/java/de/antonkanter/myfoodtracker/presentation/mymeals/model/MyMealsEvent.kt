package de.antonkanter.myfoodtracker.presentation.mymeals.model

sealed class MyMealsEvent {
    object OnInit : MyMealsEvent()
    object OnFabClicked : MyMealsEvent()
    data class OnMealItemDeleteClick(val mealItem: MealItem) : MyMealsEvent()
}
