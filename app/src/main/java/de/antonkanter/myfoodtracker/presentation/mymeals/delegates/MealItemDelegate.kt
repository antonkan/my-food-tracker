package de.antonkanter.myfoodtracker.presentation.mymeals.delegates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import de.antonkanter.myfoodtracker.R
import de.antonkanter.myfoodtracker.databinding.MyMealsMealItemViewBinding
import de.antonkanter.myfoodtracker.presentation.mymeals.model.MealItem
import de.antonkanter.myfoodtracker.presentation.mymeals.model.MyMealsItem
import de.antonkanter.myfoodtracker.util.onClick
import javax.inject.Inject
import kotlin.properties.Delegates

class MealItemDelegate @Inject constructor() : AbsListItemAdapterDelegate<
        MealItem, MyMealsItem, MealItemDelegate.MealItemViewHolder>() {

    lateinit var onDeleteClick: (MealItem) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup): MealItemViewHolder =
        LayoutInflater.from(parent.context)
            .inflate(R.layout.my_meals_meal_item_view, parent, false)
            .let { MealItemViewHolder(it) }

    override fun isForViewType(
        item: MyMealsItem,
        items: MutableList<MyMealsItem>,
        position: Int
    ): Boolean = item is MealItem

    override fun onBindViewHolder(
        item: MealItem,
        holder: MealItemViewHolder,
        payloads: MutableList<Any>
    ) = holder.bind(item, onDeleteClick)

    class MealItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val binding = MyMealsMealItemViewBinding.bind(itemView)

        private var isExpanded: Boolean by Delegates.observable(false) { _, _, newValue ->
            updateExpandIcon(newValue)
        }

        fun bind(data: MealItem, onDeleteClick: (MealItem) -> Unit) = with(binding) {
            miNameTextView.text = data.name
            miWeightValueTextView.text = itemView.context.getString(
                R.string.my_meal_weight_value,
                data.weightInG
            )
            miProteinValueTextView.text = itemView.context.getString(
                R.string.my_meal_protein_value,
                data.proteinInG
            )
            miCarbsValueTextView.text = itemView.context.getString(
                R.string.my_meal_carbs_value,
                data.carbsInG
            )
            miFatValueTextView.text = itemView.context.getString(
                R.string.my_meal_fat_value,
                data.fatInG
            )
            miIngredientsContainer.bind(data.ingredients)
            miExpandIcon.onClick { isExpanded = isExpanded.not() }
            updateExpandIcon(isExpanded)
            miDeleteIcon.onClick { onDeleteClick(data) }
        }

        private fun updateExpandIcon(isExpanded: Boolean) = with(binding) {
            miExpandIcon.setImageDrawable(
                itemView.context.getDrawable(
                    if (isExpanded) R.drawable.ic_expand_less else R.drawable.ic_expand_more
                )
            )
            miIngredientsContainer.isVisible = isExpanded
        }
    }
}
