package de.antonkanter.myfoodtracker.presentation.ingredients

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.TextView
import de.antonkanter.myfoodtracker.R

class IngredientItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    private val nameTextView: TextView
    private val valueTextView: TextView

    init {
        inflate(context, R.layout.ingredient_item_view, this)
        nameTextView = findViewById(R.id.my_meals_ingredient_item_name)
        valueTextView = findViewById(R.id.my_meals_ingredient_item_value)
    }

    fun bind(model: IngredientUiModel) {
        nameTextView.text = model.name
        valueTextView.text = context.getString(R.string.my_meals_ingredient_value, model.amountInG)
    }
}
