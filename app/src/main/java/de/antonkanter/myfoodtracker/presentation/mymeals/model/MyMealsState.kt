package de.antonkanter.myfoodtracker.presentation.mymeals.model

sealed class MyMealsState {

    object Loading : MyMealsState()

    data class Loaded(
        val items: List<MyMealsItem>
    ) : MyMealsState()
}
