package de.antonkanter.myfoodtracker.presentation.createmeal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import de.antonkanter.myfoodtracker.databinding.CreateMealFragmentBinding
import de.antonkanter.myfoodtracker.presentation.base.MviFragment
import de.antonkanter.myfoodtracker.presentation.createmeal.addingredient.AddIngredientDialogFragment
import de.antonkanter.myfoodtracker.presentation.createmeal.model.CreateMealEffect
import de.antonkanter.myfoodtracker.presentation.createmeal.model.CreateMealEvent
import de.antonkanter.myfoodtracker.presentation.createmeal.model.CreateMealState
import de.antonkanter.myfoodtracker.util.onClick
import javax.inject.Inject
import javax.inject.Named

private typealias ViewBinding = CreateMealFragmentBinding
private typealias State = CreateMealState
private typealias Effect = CreateMealEffect
private typealias Event = CreateMealEvent
private typealias ViewModel = CreateMealViewModel

@AndroidEntryPoint
class CreateMealFragment : MviFragment<ViewBinding, State, Effect, Event, ViewModel>() {

    @Inject
    @Named("supportFragmentManager")
    internal lateinit var supportFragmentManager: FragmentManager

    override val viewModel: CreateMealViewModel by activityViewModels()

    override fun inflateViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        ViewBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.createMealAddIngredientCta.onClick {
            processViewEvent(CreateMealEvent.OnAddIngredientClicked)
        }
        binding.createMealAddMealCta.onClick {
            processViewEvent(
                CreateMealEvent.OnFinishAddMeal(
                    titleInput = binding.createMealTitleTextInput.text?.toString()
                )
            )
        }
        processViewEvent(CreateMealEvent.OnInit)
    }

    override fun renderViewState(viewState: State) {
        binding.createMealIngredientsContainer.bind(viewState.ingredients)
    }

    override fun renderViewEffect(viewEffect: Effect) {
        when (viewEffect) {
            CreateMealEffect.NavigateToAddIngredient -> navigateToAddIngredientDialog()
            CreateMealEffect.ShowMissingTitleError -> showMissingTitleError()
            CreateMealEffect.ShowEmptyIngredientsError -> showEmptyIngredientsError()
            CreateMealEffect.MealAdded -> onMealAdded()
        }
    }

    private fun navigateToAddIngredientDialog() {
        AddIngredientDialogFragment
            .newInstance()
            .show(supportFragmentManager, AddIngredientDialogFragment.TAG)
    }

    private fun showMissingTitleError() {
        // TODO (#4)
    }

    private fun showEmptyIngredientsError() {
        // TODO (#4)
    }

    private fun onMealAdded() {
        findNavController().popBackStack()
        Snackbar.make(binding.createMealRoot, "Meal Added", Snackbar.LENGTH_SHORT).show()
    }
}
