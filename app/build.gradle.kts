plugins {
    id(BuildPlugins.androidApplication)
    id(BuildPlugins.kotlinAndroid)
    id(BuildPlugins.safeArgsKotlin)
    id(BuildPlugins.daggerHilt)
    id(BuildPlugins.ksp)
    kotlin(BuildPlugins.kapt)
}

android {
    compileSdk = AndroidSdk.compile
    namespace = "de.antonkanter.myfoodtracker"

    buildFeatures {
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    defaultConfig {
        applicationId = "de.antonkanter.myfoodtracker"
        minSdk = AndroidSdk.min
        targetSdk = AndroidSdk.target
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        javaCompileOptions {
            annotationProcessorOptions {
                arguments += mapOf(
                    "room.schemaLocation" to "$projectDir/schemas",
                    "room.incremental" to "true",
                    "room.expandProjection" to "true"
                )
            }
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {
    implementation(Libraries.appCompat)
    implementation(Libraries.ktxCore)
    implementation(Libraries.constraintLayout)
    implementation(Libraries.Navigation.fragment)
    implementation(Libraries.Navigation.ui)
    implementation(Libraries.Navigation.fragmentKtx)
    implementation(Libraries.Navigation.uiKtx)
    implementation(Libraries.Navigation.dynamicFeaturesFragment)
    implementation(Libraries.Navigation.hiltNav)
    testImplementation(Libraries.Navigation.testing)
    implementation(Libraries.legacySupport)
    implementation(Libraries.Lifecycle.viewmodelKtx)
    implementation(Libraries.Room.room)
    annotationProcessor(Libraries.Room.compiler)
    ksp(Libraries.Room.compiler)
    implementation(Libraries.Room.ktx)
    implementation(Libraries.DaggerHilt.hilt)
    annotationProcessor(Libraries.DaggerHilt.compiler)
    kapt(Libraries.DaggerHilt.compiler)
    implementation(Libraries.adapterDelegates)
    implementation(Libraries.materialDesign)
    testImplementation(TestLibraries.junit4)
    androidTestImplementation(TestLibraries.testRunner)
    androidTestImplementation(TestLibraries.espresso)
}
