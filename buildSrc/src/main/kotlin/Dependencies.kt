const val kotlinVersion = "1.5.31"

object BuildPlugins {

    object Versions {
        const val kotlinVersion = "1.9.10"
        const val androidLibraryVersion = "8.0.2"
        const val navigationVersion = "2.7.1"
        const val daggerHiltVersion = "2.48"
        const val kspVersion = "1.9.10-1.0.13"
        const val kaptVersion = "1.9.10"
    }

    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
    const val navigationSafeArgsPlugin =
        "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigationVersion}"
    const val daggerHiltPlugin =
        "com.google.dagger:hilt-android-gradle-plugin:${Versions.daggerHiltVersion}"
    const val androidApplication = "com.android.application"
    const val androidLibrary = "com.android.library"
    const val kotlinAndroid = "org.jetbrains.kotlin.android"
    const val safeArgsKotlin = "androidx.navigation.safeargs.kotlin"
    const val daggerHilt = "dagger.hilt.android.plugin"
    const val ksp = "com.google.devtools.ksp"
    const val kapt = "kapt"

}

object AndroidSdk {
    const val min = 26
    const val compile = 34
    const val target = compile
}

object Libraries {
    private object Versions {
        const val appcompat = "1.6.1"
        const val constraintLayout = "1.1.3"
        const val ktx = "1.2.0"
        const val room = "2.5.2"
        const val navigation = "2.7.4"
        const val legacySupport = "1.0.0"
        const val lifecycle = "2.5.1"
        const val daggerHilt = BuildPlugins.Versions.daggerHiltVersion
        const val adapterDelegates = "4.3.0"
        const val materialDesign = "1.2.0"
        const val hiltNav = "1.0.0"
    }

    const val appCompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val ktxCore = "androidx.core:core-ktx:${Versions.ktx}"

    object Room {
        const val room       = "androidx.room:room-runtime:${Versions.room}"
        const val compiler   = "androidx.room:room-compiler:${Versions.room}"
        const val ktx        = "androidx.room:room-ktx:${Versions.room}"
    }

    object Navigation {
        const val fragment = "androidx.navigation:navigation-fragment:${Versions.navigation}"
        const val ui = "androidx.navigation:navigation-ui:${Versions.navigation}"
        const val fragmentKtx =
            "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
        const val uiKtx = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
        const val dynamicFeaturesFragment =
            "androidx.navigation:navigation-dynamic-features-fragment:${Versions.navigation}"
        const val testing = "androidx.navigation:navigation-testing:${Versions.navigation}"
        const val hiltNav = "androidx.hilt:hilt-navigation-fragment:${Versions.hiltNav}"
    }

    const val legacySupport = "androidx.legacy:legacy-support-v4:${Versions.legacySupport}"

    object Lifecycle {
        const val viewmodelKtx =
            "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    }

    object DaggerHilt {
        const val hilt = "com.google.dagger:hilt-android:${Versions.daggerHilt}"
        const val compiler = "com.google.dagger:hilt-compiler:${Versions.daggerHilt}"
    }

    const val adapterDelegates = "com.hannesdorfmann:adapterdelegates4:${Versions.adapterDelegates}"
    const val materialDesign = "com.google.android.material:material:${Versions.materialDesign}"

}

object TestLibraries {
    private object Versions {
        const val junit4 = "4.12"
        const val testRunner = "1.1.0-alpha4"
        const val espresso = "3.2.0"
    }

    const val junit4 = "junit:junit:${Versions.junit4}"
    const val testRunner = "androidx.test:runner:${Versions.testRunner}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
}
